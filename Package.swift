import PackageDescription

let package = Package(
    name: "Cpng",
    pkgConfig: "libpng",
    providers: [
        .Brew("libpng"),
        .Apt("libpng-dev")
    ]
)
